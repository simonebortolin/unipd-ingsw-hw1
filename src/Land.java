public enum Land {
    MOUNTAIN {
        public String toString() {
            return "M";
        }
    }
    , PLAIN {
        public String toString() {
            return "P";
        }
    }, FOREST {
        public String toString() {
            return "F";
        }
    }
}
